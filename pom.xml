<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Licensed to the Apache Software Foundation (ASF) under one
  ~ or more contributor license agreements.  See the NOTICE file
  ~ distributed with this work for additional information
  ~ regarding copyright ownership.  The ASF licenses this file
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.douyin</groupId>
    <artifactId>douyin</artifactId>
    <version>0.1</version>

    <parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.6.RELEASE</version> 
		<relativePath />
	</parent>
	<properties>
        <!-- 文件拷贝时的编码 -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <!-- 编译时的编码 -->
        <maven.compiler.encoding>UTF-8</maven.compiler.encoding>

        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>

        <java.version>1.8</java.version>
        <djl.version>0.11.0</djl.version>
    </properties>
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-aop</artifactId>
		</dependency>
		<dependency>
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-devtools</artifactId>
 			<optional>true</optional>
 		</dependency>
		<!--web -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
            <!-- websocket -->
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-websocket</artifactId>
        </dependency>
		<!--swagger2 --> 
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>2.6.1</version>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>2.6.1</version>
		</dependency>
		<dependency>
			<groupId>com.github.xiaoymin</groupId>
			<artifactId>swagger-bootstrap-ui</artifactId>
			<version>1.8.2</version>
		</dependency>
		<!--hutool -->
		 <dependency>
            <groupId>cn.hutool</groupId>
            <artifactId>hutool-all</artifactId>
            <version>4.1.19</version>
        </dependency>
		<!--commons -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.6</version>
		</dependency>
		<dependency>
			<groupId>commons-configuration</groupId>
			<artifactId>commons-configuration</artifactId>
			<version>1.10</version>
		</dependency>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.5</version>
		</dependency>
		 <dependency>
            <groupId>commons-fileupload</groupId>
            <artifactId>commons-fileupload</artifactId>
            <version>1.4</version>
        </dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
		</dependency>
		<dependency>
			<groupId>net.sourceforge.nekohtml</groupId>
			<artifactId>nekohtml</artifactId>
		</dependency>
		<!-- 身份证照片识别 -->
		<dependency>
			<groupId>org.bytedeco</groupId>
			<artifactId>javacpp</artifactId>
			<version>1.4.3</version>
		</dependency>
		<dependency>
			<groupId>org.bytedeco</groupId>
			<artifactId>javacv</artifactId>
			<version>1.4.3</version>
		</dependency>

		<dependency>
			<groupId>org.bytedeco.javacpp-presets</groupId>
			<artifactId>opencv</artifactId>
			<version>3.4.3-1.4.3</version>
		</dependency>

		<dependency>
			<groupId>org.bytedeco.javacpp-presets</groupId>
			<artifactId>opencv-platform</artifactId>
			<version>3.4.3-1.4.3</version>
		</dependency>
		<dependency>
			<groupId>net.sourceforge.tess4j</groupId>
			<artifactId>tess4j</artifactId>
			<version>4.5.1</version>
			<exclusions>
				<exclusion>
					<groupId>log4j</groupId>
					<artifactId>log4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency> 
		<!-- 图片裁剪压缩 -->
		<dependency>
		    <groupId>net.coobird</groupId>
		    <artifactId>thumbnailator</artifactId>
		    <version>0.4.11</version>
		</dependency>
		<!-- jsqlbox -->
		<dependency>
		   <groupId>com.github.drinkjava2</groupId>
		   <artifactId>jsqlbox</artifactId>  
		   <version>4.0.6.jre8</version> <!-- 或最新版 -->
		</dependency> 
		<dependency>
		    <groupId>org.projectlombok</groupId>
		    <artifactId>lombok</artifactId>
		    <scope>provided</scope>
		</dependency>
		<!-- <dependency>
			<groupId>ojdbc6</groupId>
			<artifactId>ojdbc6</artifactId>
			<version>2.0</version>
		</dependency> -->
		<dependency>
		    <groupId>cglib</groupId>
		    <artifactId>cglib</artifactId>
		    <version>3.2.5</version>
		</dependency>
		
		<!--djl-->
       <dependency>
		    <groupId>net.java.dev.jna</groupId>
		    <artifactId>jna</artifactId>
		    <version>5.3.0</version>
		</dependency>
       <dependency>
           <groupId>ai.djl</groupId>
           <artifactId>api</artifactId>
           <version>${djl.version}</version>
       </dependency>
       <dependency>
           <groupId>ai.djl</groupId>
           <artifactId>basicdataset</artifactId>
           <version>${djl.version}</version>
       </dependency>
       <dependency>
           <groupId>ai.djl</groupId>
           <artifactId>model-zoo</artifactId>
           <version>${djl.version}</version>
       </dependency>
       <!-- MXNet -->
       <dependency>
           <groupId>ai.djl.mxnet</groupId>
           <artifactId>mxnet-model-zoo</artifactId>
           <version>${djl.version}</version>
       </dependency>
       <dependency>
           <groupId>ai.djl.mxnet</groupId>
           <artifactId>mxnet-engine</artifactId>
           <version>${djl.version}</version>
       </dependency>
       <dependency>
           <!--
               See https://github.com/deepjavalibrary/djl/blob/master/mxnet/mxnet-engine/README.md for more MXNet library selection options
           -->
           <groupId>ai.djl.mxnet</groupId>
           <artifactId>mxnet-native-auto</artifactId>
           <version>1.8.0</version>
           <scope>runtime</scope>
       </dependency>
       <!-- Pytorch -->
       <dependency>
           <groupId>ai.djl.pytorch</groupId>
           <artifactId>pytorch-engine</artifactId>
           <version>${djl.version}</version>
       </dependency>
       <dependency>
           <groupId>ai.djl.pytorch</groupId>
           <artifactId>pytorch-model-zoo</artifactId>
           <version>${djl.version}</version>
       </dependency>
       <dependency>
           <!--
               See https://github.com/deepjavalibrary/djl/blob/master/pytorch/README.md for more PyTorch library selection options
           -->
           <groupId>ai.djl.pytorch</groupId>
           <artifactId>pytorch-native-auto</artifactId>
           <version>1.8.1</version>
           <scope>runtime</scope>
       </dependency>
       <dependency>
           <groupId>org.testng</groupId>
           <artifactId>testng</artifactId>
           <version>6.8.1</version>
           <scope>test</scope>
       </dependency>
       <dependency>
           <groupId>ai.djl.paddlepaddle</groupId>
           <artifactId>paddlepaddle-engine</artifactId>
           <version>${djl.version}</version>
       </dependency>
       <dependency>
           <groupId>ai.djl.paddlepaddle</groupId>
           <artifactId>paddlepaddle-model-zoo</artifactId>
           <version>${djl.version}</version>
       </dependency>
       <dependency>
           <groupId>ai.djl.paddlepaddle</groupId>
           <artifactId>paddlepaddle-native-auto</artifactId>
           <version>2.0.2</version>
           <scope>runtime</scope>
       </dependency>
	 
		<!-- netty-socketio： 仿`node.js`实现的socket.io服务端 -->
		<dependency>
		    <groupId>com.corundumstudio.socketio</groupId>
		    <artifactId>netty-socketio</artifactId>
		    <version>1.7.19</version>
		</dependency>
		<!-- socket.io客户端 -->
		<dependency>
		    <groupId>io.socket</groupId>
		    <artifactId>socket.io-client</artifactId>
		    <version>2.0.1</version>
		</dependency>
		 <dependency>
		　　<groupId>org.java-websocket</groupId>
		　　<artifactId>Java-WebSocket</artifactId>
		　　<version>1.3.8</version>
		</dependency>
		<dependency>
		  <groupId>io.github.fanyong920</groupId>
		  <artifactId>jvppeteer</artifactId>
		  <version>1.1.3</version>
		</dependency>
		<dependency>
		    <groupId>com.alibaba</groupId>
		    <artifactId>fastjson</artifactId>
		    <version>1.2.78</version>
		</dependency>
		<dependency>
		    <groupId>net.java.dev.jna</groupId>
		    <artifactId>jna-platform</artifactId>
		    <version>5.9.0</version>
		</dependency>
 
	</dependencies>
	<build>
		<plugins>
			<plugin>
	           <artifactId>maven-resources-plugin</artifactId>
	           <executions>
	               <execution>
	                   <id>copy-xmls</id>
	                   <phase>process-sources</phase>
	                   <goals>
	                       <goal>copy-resources</goal>
	                   </goals>
	                   <configuration>
	                       <outputDirectory>target/classes</outputDirectory>
	                       <resources>
	                           <resource>
	                               <directory>src/main/resources</directory>
	                               <filtering>true</filtering>
	                               <excludes>
									   <exclude>**/*.*</exclude>
	                               </excludes>
	                           </resource>
	                           <resource>
	                               <directory>src/main/resources</directory>
	                               <filtering>false</filtering>
	                               <includes>
									   <exclude>**/*.*</exclude>
	                               </includes>
	                           </resource>
	                       </resources>
	                   </configuration>
	               </execution>
	           </executions>
	       </plugin> 
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<!--false 才能zip格式更新-->
					<executable>false</executable>
					<fork>true</fork>
					<!-- 此处是启动类-->
					<mainClass>endless.Application</mainClass>
					<!-- 要zip模式-->
					<layout>ZIP</layout>
					<includes>
						<!-- 第一次打jar包要注释掉,以后要开启
						<include>
							<groupId>nothing</groupId>
							<artifactId>nothing</artifactId>
						</include>-->
					</includes>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>repackage</goal>
						</goals>
					</execution>
				</executions>
			</plugin> 
			 
			<plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
		</plugins>
	</build>
</project>