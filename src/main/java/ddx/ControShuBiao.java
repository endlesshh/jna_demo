package ddx;

import java.awt.*;
import java.awt.event.InputEvent;
import java.io.IOException;
import java.util.Date;

/**
 * @author: zhaoxu
 * @date: 2020/9/25 16:08
 */
public class ControShuBiao implements Runnable {
    Robot rb;

    public ControShuBiao(Robot robot) {
        rb = robot;
    }

    @Override
    public void run() {
        while (true) {
            try {
            	System.out.println("鼠标监控");
                Thread.sleep(10000);
                if (System.currentTimeMillis() - ShuBiaoJianKong.last.getTime() >= 10000) {
                    //ShuBiaoJianKong.open("remote.rdp");
                    ShuBiaoJianKong.open("oracle_out.conf");
                    Thread.sleep(300);
                    ShuBiaoJianKong.close();
                    ShuBiaoJianKong.last = new Date();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            rb.mouseMove(3500, 500);
//            rb.mouseMove(3500, 300);
//            rb.mousePress(InputEvent.BUTTON1_MASK);
//            rb.keyPress(KeyEvent.VK_0);
//            rb.keyPress(KeyEvent.VK_1);
        }
    }
}
