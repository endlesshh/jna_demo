package ddx;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.ptr.IntByReference;

public class TestWin {
	private static final int MAX_TITLE_LENGTH = 1024;
	public static User32 instance = User32.INSTANCE;
	public static void main(String[] args) throws Exception {
		
		WinDef.HWND hwnd = instance.FindWindow(null, "Plants vs. Zombies");
		//显示窗口
		//instance.ShowWindow(hwnd, WinUser.SW_RESTORE);
		// 前置窗口
		instance.SetForegroundWindow(hwnd);
		WinDef.HWND fhwnd = instance.GetForegroundWindow();
		 
		System.out.println(getWindowTitle(fhwnd));
		
		WinDef.RECT rect = getRect(hwnd); 
		System.out.println(rect.top+","+rect.left+","+rect.right+","+rect.bottom);
		
		
		System.out.println(hwnd);
		/*KeyboardHook kbhook = new KeyboardHook();
		new Thread(kbhook).start();*/
		MouseHook mhook = new MouseHook();
		new Thread(mhook).start(); 
		Thread.currentThread().join();
	}
	private static String getWindowTitle(HWND window) { 
		 char[] buffer = new char[MAX_TITLE_LENGTH * 2];
         HWND hwnd = User32.INSTANCE.GetForegroundWindow();
         User32.INSTANCE.GetWindowText(hwnd, buffer, MAX_TITLE_LENGTH);  
        return  Native.toString(buffer);
    }
	
	private static String getImageName(HWND window) {
        // Get the process ID of the window
        IntByReference procId = new IntByReference();
        User32.INSTANCE.GetWindowThreadProcessId(window, procId);

        // Open the process to get permissions to the image name
        HANDLE procHandle = Kernel32.INSTANCE.OpenProcess(
                Kernel32.PROCESS_QUERY_LIMITED_INFORMATION,
                false,
                procId.getValue()
        );

        // Get the image name
        char[] buffer = new char[4096];
        IntByReference bufferSize = new IntByReference(buffer.length);
        boolean success = Kernel32.INSTANCE.QueryFullProcessImageName(procHandle, 0, buffer, bufferSize);

        // Clean up: close the opened process
        Kernel32.INSTANCE.CloseHandle(procHandle);

        return success ? new String(buffer, 0, bufferSize.getValue()) : null;
    }
	 /**
     * 获取矩形
     *
     * @param hwnd
     * @return
     */
    private static WinDef.RECT getRect(WinDef.HWND hwnd) {
        WinDef.RECT winRect = new WinDef.RECT();
        instance.GetWindowRect(hwnd, winRect);
        return winRect;
    }

    /**
     * 获取宽度
     * @param rect
     * @return
     */
    private static int getWidth(WinDef.RECT rect) {
        return rect.right - rect.left;
    }

    /**
     * 获取高度
     * @param rect
     * @return
     */
    private static int getHeight(WinDef.RECT rect) {
        return rect.bottom - rect.top;
    }
    
     /**
     * 获取宽度
     * @param hwnd
     * @return
     */
    private static int getWidth(WinDef.HWND hwnd) {
        WinDef.RECT rect = getRect(hwnd);
        return getWidth(rect);
    } 
}
