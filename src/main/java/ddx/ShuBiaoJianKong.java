package ddx;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.*;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;

import static java.lang.Thread.sleep;

/**
 * @author: zhaoxu
 * @date: 2020/9/25 15:20
 */
public class ShuBiaoJianKong implements Runnable {
    static volatile Date last = new Date();
    static volatile Date main = new Date();
    static ArrayList<String> windowText = new ArrayList<>();

    public static void close() throws IOException, InterruptedException {
//        通过窗口标题获取窗口句柄
        WinDef.HWND hWnd;
        final User32 user32 = User32.INSTANCE;
        user32.EnumWindows(new WinUser.WNDENUMPROC() {
            @Override
            public boolean callback(WinDef.HWND hWnd, Pointer arg1) {
                char[] windowText = new char[512];
                user32.GetWindowText(hWnd, windowText, 512);
                String wText = Native.toString(windowText);
                // get rid of this if block if you want all windows regardless of whether
                // or not they have text
                if (wText.isEmpty()) {
                    return true;
                }
                if (wText.contains("Google Chrome (192.168.70.241)")) {
                    ShuBiaoJianKong.windowText.add(wText);
                }
                System.out.println(wText);
                return true;
            }
        }, null);
        if (ShuBiaoJianKong.windowText.size() < 1) {
            System.out.println("找不到窗口，正在打开窗口。。。");
            ShuBiaoJianKong.windowText.add("新标签页 - Google Chrome (192.168.70.241)");
            open("remote.rdp");
        } else {
            int y = 0;
            for (String title : ShuBiaoJianKong.windowText) {
                hWnd = User32.INSTANCE.FindWindow(null, title);
                if ("新标签页 - Google Chrome (192.168.70.241)".equals(title) && y == 0) {
                    WinDef.LRESULT lresult = User32.INSTANCE.SendMessage(hWnd, 0X10, null, null);
                    System.out.println("关闭成功");
                    y++;
                } else {
                    int i = (int) (Math.random() * (3 - 2 + 1) + 2);
                    User32.INSTANCE.ShowWindow(hWnd, i);
                    User32.INSTANCE.SetForegroundWindow(hWnd);
                }
//                String username = "a";
//                for (Character c : username.toCharArray()) {
//                    sendChar(c);
//                }
            }
        }
    }

    public static void open(String path) throws IOException {
        java.awt.Desktop dp = java.awt.Desktop.getDesktop();
        if (dp.isSupported(java.awt.Desktop.Action.BROWSE)) {
            File file = new File(path);
            dp.open(file);
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("正在监控中。。。。");
        final Robot rb = new Robot();
        rb.delay(500);
        ShuBiaoJianKong jianKong = new ShuBiaoJianKong();
        new Thread(jianKong).start();
        new Thread(new ControShuBiao(rb)).start();
    }

    static WinUser.INPUT input = new WinUser.INPUT();

    static void sendChar(char ch) {
        input.type = new WinDef.DWORD(WinUser.INPUT.INPUT_KEYBOARD);
        input.input.setType("ki"); // Because setting INPUT_INPUT_KEYBOARD is not enough: https://groups.google.com/d/msg/jna-users/NDBGwC1VZbU/cjYCQ1CjBwAJ
        input.input.ki.wScan = new WinDef.WORD(0);
        input.input.ki.time = new WinDef.DWORD(0);
        input.input.ki.dwExtraInfo = new BaseTSD.ULONG_PTR(0);
        // Press
        input.input.ki.wVk = new WinDef.WORD(Character.toUpperCase(ch)); // 0x41
        input.input.ki.dwFlags = new WinDef.DWORD(0);  // keydown
        User32.INSTANCE.SendInput(new WinDef.DWORD(1), (WinUser.INPUT[]) input.toArray(1), input.size());
        // Release
        input.input.ki.wVk = new WinDef.WORD(Character.toUpperCase(ch)); // 0x41
        input.input.ki.dwFlags = new WinDef.DWORD(2);  // keyup
        User32.INSTANCE.SendInput(new WinDef.DWORD(1), (WinUser.INPUT[]) input.toArray(1), input.size());
    }

    @Override
    public void run() {
        setHookOn();
    }

    private WinUser.HHOOK hhk;

    private WinUser.LowLevelKeyboardProc mouseProc = new WinUser.LowLevelKeyboardProc() {

        @Override
        public WinDef.LRESULT callback(int nCode, WinDef.WPARAM wParam, WinUser.KBDLLHOOKSTRUCT event) {
            if (nCode >= 0) {
                ShuBiaoJianKong.last = new Date();
                if (wParam.intValue() == 512) {
                   // System.out.println("鼠标移动");
                }

                if (wParam.intValue() == 513) {
                   // System.out.println("鼠标左键按下");
                }

                if (wParam.intValue() == 514) {
                   // System.out.println("鼠标左键弹起");
                }

                if (wParam.intValue() == 516) {
                   // System.out.println("鼠标右键按下");
                }

                if (wParam.intValue() == 517) {
                   // System.out.println("鼠标右键弹起");
                }
            }
            return User32.INSTANCE.CallNextHookEx(hhk, nCode, wParam, null);
        }
    };

    public void setHookOn() {
        WinDef.HMODULE hMod = Kernel32.INSTANCE.GetModuleHandle(null);
        hhk = User32.INSTANCE.SetWindowsHookEx(User32.WH_MOUSE_LL, mouseProc, hMod, 0);
        int result;
        WinUser.MSG msg = new WinUser.MSG();
        while ((result = User32.INSTANCE.GetMessage(msg, null, 0, 0)) != 0) {
            if (result == -1) {
                setHookOff();
                break;
            } else {
                User32.INSTANCE.TranslateMessage(msg);
                User32.INSTANCE.DispatchMessage(msg);
            }
        }
    }

    public void setHookOff() {
        User32.INSTANCE.UnhookWindowsHookEx(hhk);
        System.exit(0);
    }
}
