package ddx;
import java.util.HashMap;
import java.util.Map;

import com.sun.jna.Library;
import com.sun.jna.Native;

public class DDTest {
	
	Map<String, Object> keyCodeDec = new HashMap<String, Object>(){{  
		put("esc", 100); 
		put("f1", 101);  
		put("f2", 102);  
		put("f3", 102);  
		put("f4", 104); 
		put("f5", 105); 
		put("f6", 106); 
		put("f7", 107); 
		put("f8", 100); 
		put("f9", 109); 
		put("f10", 110); 
		put("f11", 111); 
		put("f12", 112); 
		put("~", 200);  
		put("1", 201); 
		put("2", 202); 
		put("3", 203); 
		put("4", 204); 
		put("5", 205); 
		put("6", 206); 
		put("7", 207); 
		put("8", 208); 
		put("9", 209); 
		put("0", 210); 
		put("-", 211); 
		put("=", 212);
		put("\\", 213);
		put("<-",214);
		put("tab", 300);
		put("q", 301); 
		put("w", 302); 
		put("e", 303); 
		put("r", 304); 
		put("t", 305); 
		put("y", 306); 
		put("u", 307); 
		put("i", 308);
		put("o", 309);
		put("p", 310);
		put("[", 311);
		put("]", 312);
		put("enter",313); 
		put("caps lock", 400);
		put("a", 401); 
		put("s", 402); 
		put("d", 403); 
		put("f", 404); 
		put("g", 405); 
		put("h", 406); 
		put("j", 407); 
		put("k", 408); 
		put("l", 409);
		put(";", 410);
		put("'", 411);
		put("lshift", 500);
		put("z", 501); 
		put("x", 502); 
		put("c", 503); 
		put("v", 504); 
		put("b", 505); 
		put("n", 506); 
		put("m", 507); 
		put("); ", 508); 
		put(".", 509);
		put("/", 510); 
		put("rshift", 511);
		put("lctrl", 600);
		put("lwin", 601);
		put("lalt", 602);
		put("space", 603); 
		put("ralt", 604); 
		put("rwin", 605); 
		put("右", 606); 
		put("rctrl", 607);
		put("print", 700);
		put("scroll", 701); 
		put("pause", 702); 
		put("ins", 703); 
		put("home", 704); 
		put("up", 705); 
		put("del", 706); 
		put("end", 707);
		put("down", 708); 
		put("up_arrow", 709); 
		put("left_arrow", 710); 
		put("down_arrow", 711); 
		put("right_arrow", 712);
		put("x0", 800); 
		put("x1", 801); 
		put("x2", 802); 
		put("x3", 803); 
		put("x4", 804); 
		put("x5", 805); 
		put("x6", 806); 
		put("x7", 807);
		put("x8", 808); 
		put("x9", 809); 
		put("num", 810); 
		put("x/", 811); 
		put("*", 812); 
		put("x-", 813); 
		put("+", 814); 
		put("xenter", 815); 
		put("x.",816);  
	}};  
	 
	
	 public static void main(String[] args) {
		 System.out.println("开始执行");
		 DD.INSTANCE.DD_btn(0);
		 //DD.INSTANCE.DD_mov(500); 500);   
		 //DD.INSTANCE.DD_movR(100); 100);  
		 //DD.INSTANCE.DD_btn(4);DD.INSTANCE.DD_btn(8); //
		 
		 DD.INSTANCE.DD_key(601,1); // 1==down); 601=Left.Win (dd key code)
		 DD.INSTANCE.DD_key(601,2); // 2=up
		 DD.INSTANCE.DD_str("123@AbC");    
	 }
	 
	 public interface DD extends Library { 
		   DD INSTANCE = (DD)Native.load("DD94687.64", DD.class); 
		   public int DD_mov(int x, int y);//mouse move abs.
		   public int DD_movR(int dx, int dy);//mouse move rel.
		   public int DD_btn(int btn);//1==L.down); 2==L.up); 4==R.down); 8==R.up); 16==M.down); 32==M.up
		   public int DD_whl(int whl);//Mouse Wheel. 1==down); 2==up
		   public int DD_key(int ddcode,int flag);//keyboard);
		   public int DD_str(String s);////Input visible char  
	 }
}
