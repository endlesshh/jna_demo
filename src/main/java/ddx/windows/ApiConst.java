package ddx.windows;

/**
 * WinAPI的一些常量
 */
public class ApiConst {

    public static final int WM_MOUSEACTIVATE = 0x21; // 获得扩展窗口风格
    public static final int GWL_EXSTYLE = -20; // 获得扩展窗口风格
    public static final int WS_EX_NOACTIVATE = 0x8000000; // 不**窗口
    public static final int MA_NOACTIVATE = 3; // 鼠标进来不获得焦点

}
