package ddx.windows;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static boolean openLog = true; // 默认打开日志

    public static void debug(String logs) {
        if (openLog) {
            System.out.println(sdf.format(new Date()) + " [INFO] - " + logs);
        }
    }

    public static boolean isOpenLog() {
        return openLog;
    }

    public static void setOpenLog(boolean openLog) {
        Logger.openLog = openLog;
    }

    // public static void main(String[] args) {
    // Logger.debug("数据异常");
    // }
}
