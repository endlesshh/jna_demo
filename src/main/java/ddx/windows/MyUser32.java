package ddx.windows;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinDef.LPARAM;
import com.sun.jna.platform.win32.WinDef.LRESULT;
import com.sun.jna.platform.win32.WinDef.WPARAM;
import com.sun.jna.win32.StdCallLibrary.StdCallCallback;
import com.sun.jna.win32.W32APIOptions;

/**
 * 自定义的一个WinApi接口
 */
public interface MyUser32 extends Library {
    static MyUser32 INSTANCE = (MyUser32) Native.load("user32", MyUser32.class, W32APIOptions.DEFAULT_OPTIONS);

    interface WindowProc extends StdCallCallback {
        LRESULT callback(HWND hWnd, int uMsg, WPARAM wParam, LPARAM lParam);
    }

    abstract WindowProc SetWindowLongPtr(HWND hWnd, int nIndex, WindowProc dwNewLong);

    abstract WindowProc GetWindowLongPtr(HWND hWnd, int nIndex);

    abstract HWND GetFocus();
}
