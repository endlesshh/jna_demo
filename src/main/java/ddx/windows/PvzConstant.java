package ddx.windows;

public class PvzConstant {
	//阳光框框
	public static int h = 20;
	public static int yang_guang_w = 80;
	public static int yang_guang_h = 100;
	//每个单独的卡片
	public static int card_w = 50;
	public static int card_h = 70;
	//卡片选择面板
	public static int card_select_w = 460;
	public static int card_select_h = 510;
	public static int card_select_b = 23;
	public static int card_select_j = 2;
	public static int card_select_tj = 1;
	public static int card_select_button = 60; //上下间隔5
	public static int card_select_top = 30; //上下间隔5
	
	//战场的分布
	public static int war_out_w = 40;  
	public static int war_out_h = 110;  
	public static int war_card_w = 80;  
	public static int war_card_h = 100;  
	
}
