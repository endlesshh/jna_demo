package client;

import java.net.URI;
import java.util.Map;

import com.google.common.collect.Maps;

public class MyTest{

	public static void main(String[] arg0) throws Exception{ 
		Map<String, String> httpHeaders = Maps.newHashMap(); 
		httpHeaders.put("roomid", "456665459766");
		//MyWebSocketClient myClient = new MyWebSocketClient(new URI("http://192.168.0.100:8001"),httpHeaders);
		MyWebSocketClient myClient = new MyWebSocketClient(new URI("http://192.168.0.100:8080/test?roomid=456665459766"));
		//MyWebSocketClient myClient = new MyWebSocketClient(new URI("http://192.168.0.100:8001"));
		myClient.connect();
		//myClient.send("此为要发送的数据内容");
		Thread.currentThread().join();
	} 
}