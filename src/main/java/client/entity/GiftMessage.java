package client.entity;

import lombok.Data;

@Data
public class GiftMessage {
	private String	  method;      
	private String    roomId;      
	private String    userId;      
	private String    nickName;    
	private String    avatarThumb; 
	private String    giftId;      
	private String    giftName;    
	private String    giftCount; 
}
