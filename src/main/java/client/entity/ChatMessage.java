package client.entity;

import lombok.Data;

@Data
public class ChatMessage {
	private String	method; 			
	private String  roomId; 			
	private String  userId; 			
	private String  nickName;			
	private String  avatarThumb;	
	private String  content;  
}
