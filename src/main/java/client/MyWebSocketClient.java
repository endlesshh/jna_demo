package client;

import java.net.URI;
import java.util.Map;

import org.apache.log4j.Logger;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

public class MyWebSocketClient extends WebSocketClient {

	Logger logger = Logger.getLogger(MyWebSocketClient.class);

	public MyWebSocketClient(URI serverUri,Map<String, String> httpHeaders) {
		super(serverUri,httpHeaders);
	}

	public MyWebSocketClient(URI serverUri) {
		super(serverUri);
	}
	
	@Override
	public void onOpen(ServerHandshake arg0) {
		// TODO Auto-generated method stub
		logger.info("------ MyWebSocket onOpen ------");
	}

	@Override
	public void onClose(int arg0, String arg1, boolean arg2) {
		// TODO Auto-generated method stub
		System.out.println(arg0);
		System.out.println(arg1);
		logger.info("------ MyWebSocket onClose ------");
	}

	@Override
	public void onError(Exception arg0) {
		// TODO Auto-generated method stub
		arg0.printStackTrace();
		logger.info("------ MyWebSocket onError  ------");
	}

	@Override
	public void onMessage(String arg0) {
		 // TODO Auto-generated method stub
		logger.info("-------- 接收到服务端数据： " + arg0 + "--------");
	}
}