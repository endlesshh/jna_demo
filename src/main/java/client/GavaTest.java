package client;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.CharMatcher;

public class GavaTest {
	public static void main(String[] args) {
		String str = "电脑 还是手机可以玩这个"; 
		str = getNbr(str).replace(" ", "");
		System.out.println("=-=-=-"+str+"=-=-=-");  
        System.out.println(StringUtils.isNotEmpty(str)); 
	}
	static String getNbr(String str) 
    { 
        // Replace each non-numeric number with a space
        str = str.replaceAll("[^\\d]", " "); 
        // Remove leading and trailing spaces
        str = str.trim(); 
        // Replace consecutive spaces with a single space
        str = str.replaceAll(" +", ","); 
  
        return str; 
    } 
  
     
}
